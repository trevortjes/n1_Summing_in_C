#include <stdio.h>
#include <stdint.h>

int num1, num2, sum;

int main()
{
   printf("First number: ");
   scanf("%d", &num1);
   printf("Second number: ");
   scanf("%d", &num2);

    sum = num1 + num2;

    printf("%d + %d = %d", num1, num2, sum);

   return 0;
}
